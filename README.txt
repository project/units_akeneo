# Disclaimer

Config file (json) has been generated from 'akeneo/MeasureBundle' project.

https://github.com/akeneo/MeasureBundle

Conversion between Yaml and Json has been done thanks for : http://yamltojson.com/

# Sources

## Units of measures

https://github.com/akeneo/MeasureBundle/blob/master/Resources/config/measure.yml

## Translations

https://github.com/akeneo/MeasureBundle/blob/master/Resources/translations/messages.en.yml
